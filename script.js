let isPhoneNo = false;

const canvas = new fabric.Canvas("canvas");

let items;

const moveable = (top) => {
  return new fabric.Text("=", {
    originX: "center",
    left: -20,
    top: top,
    lockMovementY: true,
    lockMovementX: true,
    hasBorders: false,
    hasControls: false,
  });
};
const textFeild = (value, top) => {
  const result = new fabric.Text(value, {
    top: top,
    fontSize: 15,
    lockMovementY: true,
    lockMovementX: true,
    hasBorders: false,
    hasControls: false,
    originY: "top",
  });
  return result;
};

const inputFeild = (top, value = "") => {
  const result = new fabric.Textbox(value, {
    width: 200,
    top: top,
    fontSize: 20,
    backgroundColor: "#F5F5F5",
    editable: true,
    lockMovementY: true,
    lockMovementX: true,
    hasBorders: false,
    hasControls: false,
    originY: "top",

    // hoverCursor : "text "
  });
  return result;
};
const emailId = textFeild("Email Id", 10);
const password = textFeild("Password", 65);
const phoneNo = textFeild("Phone Number", 10);

const emailIdInput = inputFeild(35);
const passwordInput = inputFeild(85);
const phoneInput = inputFeild(30);
const or = textFeild("Or", 110);
// or.set({hoverCursor : "pointer"})
const inputs = [emailIdInput, passwordInput, phoneInput];

const login = new fabric.Rect({
  width: 200,
  height: 20,
  fill: "#F5F5F5",
  originX: "center",
  originY: "center",
  editable: true,
  lockMovementY: true,
  lockMovementX: true,
  hasBorders: false,
  hasControls: false,
  strokeWidth: 1,
  stroke: "black",
});

const loginText = new fabric.Text("Log In", {
  fontSize: 18,
  originX: "center",
  originY: "center",
  editable: true,
  lockMovementY: true,
  lockMovementX: true,
  hasBorders: false,
  hasControls: false,
});
const moveable1 = moveable(30);
const moveable2 = moveable(15);
moveable1.set({hoverCursor : "grab"})
moveable2.set({hoverCursor : "grab"})

let emailGrp = new fabric.Group(
  [emailId, emailIdInput, password, passwordInput, moveable1],
  {
    left: 200,
    originX: "center",
    subTargetCheck: true,
    lockMovementY: true,
    lockMovementX: true,
    stroke : 'black' ,
    strokeDashArray : "-",
      hasControls : false,
      borderColor : 'black',
      borderDashArray: [4, 4],
  }
);

let phoneGrp = new fabric.Group([phoneNo, phoneInput, moveable2], {
  subTargetCheck: true,
  top: 150,
  left: 200,
  originX: "center",
  lockMovementY: true,
  lockMovementX: true,
  visible : false,
  stroke : 'black' ,
  strokeDashArray : "-",
  hasControls : false,
  borderColor : 'black',
  borderDashArray: [4, 4],
});
const phoneGrpOriginal = phoneGrp

const newUser = new fabric.Text("New User?", {
  fontSize: 15,
  lockMovementY: true,
  lockMovementX: true,
  hasBorders: false,
  hasControls: false,
  originX: "left",
  originY: "top",
});
const signUp = new fabric.Text("Sign Up ", {
  fontSize: 15,
  lockMovementY: true,
  lockMovementX: true,
  hasBorders: false,
  hasControls: false,
  left: 80,
  underline: true,
});


const loginButtonTopInitial = emailGrp.top + emailGrp.height + 10 ;

const loginButton = new fabric.Group([login, loginText], {
  subTargetCheck: true,
  top: loginButtonTopInitial,
  left: 200,
  originX: "center",
  lockMovementX: true,
  lockMovementY: true,
});
const signGrpTopInitial = loginButtonTopInitial + loginButton.height + 10 ;

const signGrp = new fabric.Group([newUser, signUp], {
  subTargetCheck: true,
  top: signGrpTopInitial,
  left: 200,
  originX: "center",
  lockMovementX: true,
  lockMovementY: true,
});

or.set({
  left: 200,
  visible : false
});
const loginButtonTopFinal = loginButtonTopInitial + phoneGrp.height+ 10 +   or.height + 10;
const signGrpTopFinal = loginButtonTopFinal + loginButton.height + 10 ;

const groups = [emailGrp , or, phoneGrp,loginButton, signGrp]
groups.forEach(grp =>{
  if(grp === emailGrp || grp === phoneGrp){
    return 
    

  }else{
    grp.set({ 
      lockMovementY : true,
      lockMovementX : true,
      hasBorders : false ,
      hasControls : false,
    })
  }
})
canvas.add(...groups);

let isEditable = false;
let isDragable = false;
let isStartDragable = true;




const ungroupOnly = (grp) => {
  let items = grp._objects;
  grp._restoreObjectsState();
  canvas.remove(grp);
  for (var i = 0; i < items.length; i++) {
    canvas.add(items[i]);
  }
  canvas.renderAll();

  return items;
};

let emailGrpItems = null;
let phoneGrpItems = null;

const makeDragable = (bool=true) => {
  /// make unable dragable 
  if(bool===false){
    if(emailGrp!=null){
      emailGrp.set({ lockMovementY: true });
      phoneGrp.set({ lockMovementY: true });
      isDragable = false
     
    }
    return ;
  }
   /// if items are null
  if (emailGrpItems === null) {
    emailGrp.set({ lockMovementY: false });
    phoneGrp.set({ lockMovementY: false });
    isDragable = true
  }else{
    for (var i = 0; i < emailGrpItems.length; i++) {
      canvas.remove(emailGrpItems[i]);
    }
    for (var i = 0; i < phoneGrpItems.length; i++) {
      canvas.remove(phoneGrpItems[i]);
    }
    emailGrp = new fabric.Group(emailGrpItems, {
      subTargetCheck: true,
      originX: "center",
      lockMovementX: true,
      stroke : 'black' ,
  strokeDashArray : "-", hasControls : false,
  borderColor : 'black',
  borderDashArray: [4, 4],
    });
    phoneGrp = new fabric.Group(phoneGrpItems, {
      subTargetCheck: true,
      originX: "center",
      lockMovementX: true,
      stroke : 'black' ,
  strokeDashArray : "-", hasControls : false,
  borderColor : 'black',
  borderDashArray: [4, 4],
    });
    if(!isPhoneNo){
      phoneGrp.set({visible : false})
    }
    canvas.add(emailGrp);
    canvas.add(phoneGrp);
    canvas.renderAll();
    isDragable = true;

  }
};
const makeEditable = () => {
  emailGrpItems = ungroupOnly(emailGrp);
  phoneGrpItems = ungroupOnly(phoneGrp);
  if(!isPhoneNo){
  phoneGrpItems.forEach(item =>
    item.set({visible : false}))
  }
  phoneGrp = emailGrp = null; 
  isEditable = true;
};

canvas.on("mouse:down", (e) => {
  let target = e.target;
  if (!target) {
    return;
  }
  if (e.subTargets.length != 0) {
    target = e.subTargets[0];
  }
  if(target.text === "=") {
    if(!isPhoneNo){
      return  
    }
    if(isDragable===false){
      makeDragable();
      // target.set({hoverCursor : ""})
    }
    isEditable = false;
  } else {
    makeDragable(false)
    if (!isEditable) {
      makeEditable();
      isEditable = true;
      isDragable = false;
    }
  }
});



let topGrp = null;
let bottomGrp = null
let movingObj =null ;
let nonMovingObj =null ;
let topInitials , bottomInitials;
let finalTop;
const getTopGrp=()=>{
  if(emailGrp.top<phoneGrp.top){
    topGrp=emailGrp;
    bottomGrp = phoneGrp
  }else{
    topGrp= phoneGrp
    bottomGrp=emailGrp;
  }
  topInitials = topGrp.top;
  bottomInitials = bottomGrp.top;
}

canvas.on('object:moving' , (e)=>{
  if(topGrp===null){
    getTopGrp()
  }
  e.target.setCoords();
  // e.target.set({hoverCursor : "grabbing"})
  movingObj = e.target;
  if(movingObj===emailGrp){
    nonMovingObj = phoneGrp
  }else{
    nonMovingObj=emailGrp
  }

  if(e.target===topGrp){
    if((e.target.top+e.target.height) >= nonMovingObj.top){
      makeDragable(false)
      nonMovingObj.top = 10 ;
      or.top = nonMovingObj.top + nonMovingObj.height + 10 ;
      finalTop = or.top + or.height + 10 ;
      console.log(canvas._objects)
      return
    }
    // if(e.target.top <= 5){
    //   makeDragable(false)
    //   finalTop = 10;
    // }

  }else{
    if(e.target.top <=50 ){
      makeDragable(false)
      or.top = 10 + movingObj.height + 10 ;
      nonMovingObj.top = 10 + movingObj.height + 10 + or.height + 10;
      finalTop = 10
      console.log(canvas._objects)
        return 
    } 
    
    // if(e.target.top+e.target.height>= 210){
    //   makeDragable(false)
    //   finalTop = or.top + or.height + 19 ;
    //   return
    // }
  }
})
  
canvas.on('object:moved',(e)=>{
  movingObj.top = finalTop
  topGrp = bottomGrp = movingObj=nonMovingObj= finalTop= null;
})



  document.addEventListener('click',(e)=>{
    if(e.target.tagName !='CANVAS'){
      // makeDragable()
    }
  })

let emailGrpTopNow = 0;

function changePhoneNo(e){
  isPhoneNo = !isPhoneNo;
  let emailGrpNow = emailGrp;
  let phoneGrpNow = phoneGrp;
  let loginButtonNow = loginButton
  let orNow = or;
  let signGrpNow = signGrp
  canvas._restoreObjectsState;
  if(!isPhoneNo){
    
    phoneGrpNow.set({visible : false});
    orNow.set({visible : false})
    emailGrpNow.set({top : 10})
    loginButtonNow.top = loginButtonTopInitial;
    signGrpNow.top = signGrpTopInitial ;
    canvas.renderAll()
  }else{
    if(phoneGrpNow){
      phoneGrpNow.set({visible : true});
    }else{
      phoneGrp= phoneGrpOriginal;
      phoneGrp.set({visible : true})
    }
    if(phoneGrpItems){
      phoneGrpItems.forEach(item=>{
        item.set({visible : true})
      })
    }
    orNow.set({visible : true})
    loginButtonNow.top = loginButtonTopFinal;
    signGrpNow.top = signGrpTopFinal
    if(emailGrpTopNow!=0){
      emailGrpNow.set({top : emailGrpTopNow});
      emailGrpTopNow = 0;

    }
    canvas.renderAll( )

  }
}
